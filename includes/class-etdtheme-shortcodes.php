<?php
/**
 * Shortcodes
 */

defined( 'ABSPATH' ) || exit;

/**
 * EtdTheme Shortcodes class.
 */
class EtdTheme_Shortcodes {

	/**
	 * Init shortcodes.
	 */
	public static function init() {
		$shortcodes = array( 
			'example' => __CLASS__ . '::example',
        );

		foreach ( $shortcodes as $shortcode => $function ) {
			add_shortcode( apply_filters( "{$shortcode}_shortcode_tag", $shortcode ), $function );
		}
	}
    
    public static function example() {
        global $post;
        
        return 'example';
    }

	/**
	 * Shortcode Wrapper.
	 */
	public static function shortcode_wrapper(
		$function,
		$atts = array(),
        $wrapper = array(
            'class'  => 'etdtheme',
            'before' => null,
            'after'  => null,
        )
    ) {
		ob_start();

		// @codingStandardsIgnoreStart
		echo empty( $wrapper['before'] ) ? '<div class="' . esc_attr( $wrapper['class'] ) . '">' : $wrapper['before'];
		call_user_func( $function, $atts );
		echo empty( $wrapper['after'] ) ? '</div>' : $wrapper['after'];
		// @codingStandardsIgnoreEnd

		return ob_get_clean();
	}
}
