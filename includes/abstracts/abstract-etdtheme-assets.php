<?php
/**
 * Assets base class.
 */

defined( 'ABSPATH' ) || exit;

/**
 * Abstract Assets Class.
 */
abstract class EtdTheme_Assets {

	/**
	 * Contains an array of script handles registered.
	 */
	public $registered_scripts = array();
    
	/**
	 * Contains an array of style handles registered.
	 */
	public $registered_styles = array();
    
    /**
	 * Get scripts.
	 */
	public function get_local_scripts() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		return array();
	}

	/**
	 * Get styles.
	 */
	public function get_local_styles() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		return array();
	}
    
    /**
	 * Get scripts to unload.
	 */
	public function get_local_unload_scripts() {
		return array();
	}
    
    /**
	 * Get styles to unload.
	 */
	public function get_local_unload_styles() {
		return array();
	}

	/**
	 * Return data for script handles.
	 */
	public function get_local_script_data( $handle ) {
        global $post;
        
		switch ( $handle ) {
			default :
				return array();
		}
		return false;
	}
    
	/**
	 * Enqueue the scripts when we want them.
	 */
    public function enqueue_scripts() {
    }
    
	/**
	 * Enqueue the styles when we want them.
	 */
    public function enqueue_styles() {
    }

	/**
	 * Register/queue assets.
	 */
	public function load_assets() {
        // Register scripts.
        if ( is_array( $scripts = $this->get_local_scripts() ) ) {
            foreach ( $scripts as $name => $args ) {
                $this->register_script( $name, $args['src'], $args['deps'], $args['version'] );
            }
        }
        
		// Register styles.
        if ( is_array( $styles = $this->get_local_styles() ) ) {
            foreach ( $styles as $handle => $args ) {
                $this->register_style( $handle, $args['src'], $args['deps'], $args['version'], $args['media'] );
            }
        }
        
        $this->enqueue_scripts();
        $this->enqueue_styles();
	}

	/**
	 * Return protocol relative asset URL.
	 */
	public function get_asset_url( $path ) {
        return get_stylesheet_directory_uri() . '/assets/' . $path;
	}

	/**
	 * Register and enqueue a script for use.
	 */
	public function register_script( $handle, $path = '', $deps = array( 'jquery' ), $version = ETDTHEME_VERSION, $in_footer = true ) {
		if ( !empty( $handle ) ) {
	        if ( ! in_array( $handle, $this->registered_scripts ) ) {
	            $this->registered_scripts[] = $handle;
	            wp_register_script( $handle, $path, $deps, $version, $in_footer );
	        }
		}
	}

	/**
	 * Register a styles for use.
	 */
	public function register_style( $handle, $path = '', $deps = array(), $version = ETDTHEME_VERSION, $media = 'all', $has_rtl = false ) {
		if ( !empty( $handle ) ) {
	        if ( ! in_array( $handle, $this->registered_styles ) ) {
	            $this->registered_styles[] = $handle;
	            wp_register_style( $handle, $path, $deps, $version, $media );

	            if ( $has_rtl ) {
	                wp_style_add_data( $handle, 'rtl', 'replace' );
	            }
	        }
		}
	}

	/**
	 * Localize scripts only when enqueued.
	 */
	public function localise_scripts() {
		if ( ! empty( $this->registered_styles ) ) {
			foreach ( ( array ) $this->registered_styles as $handle ) {
				$this->localise_script( $handle );
			}
		}
	}
    
	/**
	 * Localise a script.
	 */
	public function localise_script( $handle ) {
		if ( wp_script_is( $handle ) && ( $data = $this->get_local_script_data( $handle ) ) ) {
			$this->localised_scripts[] = $handle;
			$name = str_replace( '-', '_', $handle ) . '_params';
			wp_localize_script( $handle, $name, $data );
		}
	}

	/**
	 * Unload all un-wanted scripts.
	 */
	 public function unload_scripts() {
        $unload_scripts = $this->get_local_unload_scripts();
		if ( ! empty( $unload_scripts ) ) {
            foreach ( ( array ) $unload_scripts as $name ) {
                wp_dequeue_script( $name );
                wp_deregister_script( $name );
            }
        }
	}

	/**
	 * Unload all un-wanted styles.
	 */
	public function unload_styles() {
        $unload_styles = $this->get_local_unload_styles();
        if ( ! empty( $unload_styles ) ) {
			foreach ( ( array ) $unload_styles as $handle ) {
                wp_dequeue_style( $handle );
                wp_deregister_style( $handle );
			}
		}
	}
}
