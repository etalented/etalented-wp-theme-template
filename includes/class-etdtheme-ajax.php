<?php
/**
 * AJAX Event Handlers.
 */

defined( 'ABSPATH' ) || exit;

/**
 * EtdTheme_Ajax class.
 */
class EtdTheme_Ajax {

	/**
	 * Hook in ajax handlers.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'define_ajax' ), 0 );
		add_action( 'template_redirect', array( __CLASS__, 'do_ajax' ), 0 );
		self::add_ajax_events();
	}

	/**
	 * Get Ajax Endpoint.
	 */
	public static function get_endpoint( $request = '' ) {
		return esc_url_raw( add_query_arg( 'etdtheme-ajax', $request, home_url( '/', 'relative' ) ) );
	}

	/**
	 * Set AJAX constant and headers.
	 */
	public static function define_ajax() {
		if ( ! empty( $_GET['etdtheme-ajax'] ) ) {
			etdtheme_maybe_define_constant( 'DOING_AJAX', true );
			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {
				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON.
			}
			$GLOBALS['wpdb']->hide_errors();
		}
	}

	/**
	 * Send headers for Ajax Requests.
	 */
	private static function ajax_headers() {
		send_origin_headers();
		@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
		@header( 'X-Robots-Tag: noindex' );
		send_nosniff_header();
		nocache_headers();
		status_header( 200 );
	}

	/**
	 * Check for Ajax request and fire action.
	 */
	public static function do_ajax() {
		global $wp_query;

		if ( ! empty( $_GET['etdtheme-ajax'] ) ) {
			$wp_query->set( 'etdtheme-ajax', sanitize_text_field( wp_unslash( $_GET['etdtheme-ajax'] ) ) );
		}

		$action = $wp_query->get( 'etdtheme-ajax' );

		if ( $action ) {
			self::ajax_headers();
			$action = sanitize_text_field( $action );
			do_action( 'etdtheme_ajax_' . $action );
			wp_die();
		}
	}

	/**
	 * Hook in methods - uses WordPress ajax handlers (admin-ajax).
	 */
	public static function add_ajax_events() {
		// woocommerce_EVENT => nopriv.
		$ajax_events = array();

		foreach ( $ajax_events as $ajax_event => $nopriv ) {
			add_action( 'wp_ajax_etdtheme_' . $ajax_event, array( __CLASS__, $ajax_event ) );

			if ( $nopriv ) {
				add_action( 'wp_ajax_nopriv_etdtheme_' . $ajax_event, array( __CLASS__, $ajax_event ) );

				add_action( 'etdtheme_ajax_' . $ajax_event, array( __CLASS__, $ajax_event ) );
			}
		}
	}
}

EtdTheme_Ajax::init();
