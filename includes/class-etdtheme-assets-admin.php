<?php
/**
 * Handle admin assets (scripts & styles).
 */

defined( 'ABSPATH' ) || exit;

/**
 * EtdTheme_Assets_Admin Class.
 */
final class EtdTheme_Assets_Admin extends EtdTheme_Assets {

	/**
	 * Hook in methods.
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'load_assets' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'localise_scripts' ) );
    }
    

	/**
	 * Get scripts.
	 */
	public function get_local_scripts() {
		return array();
	}

	/**
	 * Get styles.
	 */
	public function get_local_styles() {
		return array();
	}

	/**
	 * Get unload styles.
	 */
	public function get_local_unload_styles() {
		return array();
	}

	/**
	 * Return data for script handles.
	 */
	public function get_local_script_data( $handle ) {
        global $post;
        
		switch ( $handle ) {
			default:
			break;
		}
		return false;
	}
    
	/**
	 * Enqueue the scripts when we want them.
	 */
    public function enqueue_scripts() {
    }
    
	/**
	 * Enqueue the styles when we want them.
	 */
    public function enqueue_styles() {
    }
}

return new EtdTheme_Assets_Admin();