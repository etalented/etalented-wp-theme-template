<?php
/**
 * Handle frontend assets (scripts & styles).
 */

defined( 'ABSPATH' ) || exit;

/**
 * EtdTheme_Assets_Frontend Class.
 */
final class EtdTheme_Assets_Frontend extends EtdTheme_Assets {

	/**
	 * Hook in methods.
	 */
	public function __construct() {
		add_action( 'wp_print_scripts', array( $this, 'unload_scripts' ), 100 );
		add_action( 'wp_print_styles', array( $this, 'unload_styles' ), 100 );
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ), 30 );
		add_action( 'wp_print_scripts', array( $this, 'localise_scripts' ), 5 );
    }

	/**
	 * Get scripts.
	 */
	public function get_local_scripts() {
		return array(
            'etdtheme-script' => array(
                'src'     => $this->get_asset_url( 'js/front/common.js' ),
                'deps'    => [ 'jquery' ],
				'version' => null,
            ),
		);
	}

	/**
	 * Get styles.
	 */
	public function get_local_styles() {
		return array(
			'etdtheme-style' => array(
				'src'     => $this->get_asset_url( 'css/front/common.css' ),
				'deps'    => [],
				'version' => null,
				'media'   => 'all',
			),
		);
	}

	/**
	 * Get unload styles.
	 */
	public function get_local_unload_styles() {
		return array();
	}

	/**
	 * Return data for script handles.
	 */
	public function get_local_script_data( $handle ) {
        global $post;
        
		switch ( $handle ) {
			default:
			break;
		}
		return false;
	}
    
	/**
	 * Enqueue the scripts when we want them.
	 */
    public function enqueue_scripts() {
        wp_enqueue_script( 'etdtheme-script' );
    }
    
	/**
	 * Enqueue the styles when we want them.
	 */
    public function enqueue_styles() {
		wp_enqueue_style( 'etdtheme-style' );
    }
}

return new EtdTheme_Assets_Frontend();