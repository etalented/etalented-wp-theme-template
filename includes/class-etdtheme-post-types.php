<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 */

defined( 'ABSPATH' ) || exit;

/**
 * EtdTheme_Post_Types Class.
 */
class EtdTheme_Post_Types {

	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'register_post_types' ), 5 );
		add_filter( 'gutenberg_can_edit_post_type', array( __CLASS__, 'gutenberg_can_edit_post_type' ), 10, 2 );
	}

	/**
	 * Register core post types.
	 */
	public static function register_post_types() {
		if ( ! is_blog_installed() || post_type_exists( 'example' ) ) {
			return;
		}
		
		$singular_name = _x( 'example', 'etdtheme', 'post type singluar name' );
		$plural_name = _x( 'examples', 'etdtheme', 'post type plural name' );

		register_post_type(
			$singular_name,
            array(
                'labels'              => array(
                    'name'                  => $singular_name,
                    'singular_name'         => $singular_name,
                    'all_items'             => sprintf( _x( 'All %s', 'etdtheme', 'post type all_items' ), $plural_name ),
                    'menu_name'             => $plural_name,
                    'add_new'               => _x( 'Add', 'etdtheme', 'post type add_new' ),
                    'add_new_item'          => sprintf( _x( 'Add %s', 'etdtheme', 'post type add_new_item' ), $singular_name ),
                    'edit'                  => _x( 'Edit', 'etdtheme', 'post type edit' ),
                    'edit_item'             => sprintf( _x( 'Edit %s', 'etdtheme', 'post type edit_item' ), $singular_name ),
                    'new_item'              => sprintf( _x( 'New %s', 'etdtheme', 'post type new_item' ), $singular_name ),
                    'view_item'             => sprintf( _x( 'View %s', 'etdtheme', 'post type view_item' ), $singular_name ),
                    'view_items'            => sprintf( _x( 'View %s', 'etdtheme', 'post type view_items' ), $plural_name ),
                    'search_items'          => sprintf( _x( 'Search %s', 'etdtheme', 'post type search_items' ), $plural_name ),
                    'not_found'             => sprintf( _x( 'No %s found', 'etdtheme', 'post type not_found' ), $plural_name ),
                    'not_found_in_trash'    => sprintf( _x( 'No %s found in the trash', 'etdtheme', 'post tpye not_found_in_trash' ), $plural_name ),
                    'parent'                => '',
                ),
                'description'         => '',
                'public'              => true,
                'show_ui'             => true,
                'capability_type'     => 'post',
                'map_meta_cap'        => true,
                'publicly_queryable'  => true,
                'exclude_from_search' => false,
                'hierarchical'        => false, // Hierarchical causes memory issues - WP loads all records!
                'rewrite'             => array(
                    'slug'       => $singular_name,
                    'with_front' => true,
                ),
                'query_var'           => true,
                'supports'            => array( 'title', 'excerpt', 'thumbnail', 'comments' ),
                'has_archive'         => false,
                'show_in_nav_menus'   => true,
                'show_in_rest'        => false,
            )
		);
	}

	/**
	 * Disable Gutenberg for post types.
	 */
	public static function gutenberg_can_edit_post_type( $can_edit, $post_type ) {
		return 'example' === $post_type ? false : $can_edit;
	}
}

EtdTheme_Post_types::init();
