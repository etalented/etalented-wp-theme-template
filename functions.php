<?php

defined( 'ABSPATH' ) || exit;

final class EtalentedTheme {
    
	public $version = '1.0.0';
    
	protected static $_instance = null;
    
    public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
    
    private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}
    
    private function is_request( $type ) {
		switch ( $type ) {
			case 'admin' :
				return is_admin();
			case 'ajax' :
				return defined( 'DOING_AJAX' );
			case 'cron' :
				return defined( 'DOING_CRON' );
			case 'frontend' :
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
		}
	}
    
    private function __construct() {
		$this->define_constants();
		$this->includes();
		$this->init_hooks();
	}
    
    private function define_constants() {
		$this->define( 'ETDTHEME_ABSPATH', dirname( __FILE__ ) . '/' );
		$this->define( 'ETDTHEME_VERSION', $this->version );
	}
    
    private function includes() {
        include_once( ETDTHEME_ABSPATH . 'includes/abstracts/abstract-etdtheme-assets.php' );
        include_once( ETDTHEME_ABSPATH . 'includes/etdtheme-core-functions.php' );
        // include_once( ETDTHEME_ABSPATH . 'includes/etdtheme-template-functions.php' );
        // include_once( ETDTHEME_ABSPATH . 'includes/class-etdtheme-ajax.php' );
        // include_once( ETDTHEME_ABSPATH . 'includes/class-etdtheme-shortcodes.php' );
        // include_once( ETDTHEME_ABSPATH . 'includes/class-etdtheme-post-types.php' );
        
        if ( $this->is_request( 'frontend' ) ) {
            include_once( ETDTHEME_ABSPATH . 'includes/class-etdtheme-assets-frontend.php' );
		}
        
        if ( $this->is_request( 'admin' ) ) {
            // include_once( ETDTHEME_ABSPATH . 'includes/class-etdtheme-assets-admin.php' );
		}
    }

    private function init_hooks() {
		add_action( 'init', array( 'EtdTheme_Shortcodes', 'init' ) );
		add_action( 'after_setup_theme', array( $this, 'add_image_sizes' ) );
    }
    
	public function add_image_sizes() {
	}
    
	public function ajax_url() {
		return admin_url( 'admin-ajax.php', 'relative' );
	}
}

function etalentedtheme() {
	return EtalentedTheme::instance();
}

// Global for backwards compatibility.
$GLOBALS['etdtheme'] = etalentedtheme();